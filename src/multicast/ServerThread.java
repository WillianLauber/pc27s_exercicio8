/*
 * Exemplo de Programação Multicast
 *
 * Autor: Lucio Agostinho Rocha
 * Ultima atualizacao: 20/10/2017
 *
 * (Baseado em: http://docs.oracle.com/javase/tutorial/networking/datagrams/examples/MulticastClient.java)
 *
*/
package multicast;

import java.io.*;
import java.net.*;
import java.util.*;

public class ServerThread extends QuoteServerThread {

    private long FIVE_SECONDS = 5000;

    StringBuilder sb = new StringBuilder();
    Scanner in = new Scanner(new FileReader("/home/willianlauber/workspace/pc27s_exercici8/src/multicast/NOTICIAS.txt"));


    public ServerThread() throws IOException {
        super("MulticastServerThread");
    }

    //Sobrecarrega o método da superclasse
    public void run() {



        while (moreQuotes) {

            try {
                byte[] buf = new byte[256];

                // construct quote
                String dString = null;
                if (in == null)
                    dString = getNextQuote();
                else
                    dString = getNextQuote();
                System.out.println(dString);
                buf = dString.getBytes();

                // send it
                InetAddress group = InetAddress.getByName("230.0.0.1");
                DatagramPacket packet = new DatagramPacket(buf, buf.length, group, 4447);
                socket.send(packet);

                // sleep for a while
                try {
                    sleep((long) (Math.random() * FIVE_SECONDS));
                } catch (InterruptedException e) {
                }
            } catch (IOException e) {
                e.printStackTrace();
                moreQuotes = false;
            }

        }

        socket.close();
    }
public String getNextQuote(){

    //while(in.hasNext()) {
    sb.append(in.next());
    return sb.toString();
}
}